create or replace PROCEDURE INSERT_W_BODY_BATCH 
(
  CODCOLONNA IN NUMBER,
  CODICECAMPO IN NUMBER, 
  MAXNUM IN NUMBER,
  PROT IN NUMBER
) AS 
    maxnum2   INTEGER;
BEGIN
    INSERT INTO w_body_batch
        SELECT protocollo,
               -4 AS codiceutente,
               -1 AS codiceprog,
               CODCOLONNA AS codicecolonna,
               num AS riga,
               CASE contenuto WHEN '-' THEN ' ' ELSE contenuto END AS contenuto
          FROM w_campi_batch
         WHERE protocollo = PROT AND codice_campo = CODICECAMPO;

    SELECT COUNT (num)
      INTO maxnum2
      FROM w_campi_batch
     WHERE protocollo = PROT AND codice_campo = CODICECAMPO;

    FOR n IN maxnum2 .. maxnum
    LOOP
        INSERT INTO w_body_batch (protocollo,
                                  codiceutente,
                                  codiceprog,
                                  codicecolonna,
                                  riga,
                                  contenuto)
             VALUES (PROT,
                     -4,
                     -1,
                     CODCOLONNA,
                     n,
                     ' ');
    END LOOP;
END INSERT_W_BODY_BATCH;

create or replace TRIGGER "AI_W_BODY_BATCH_EC" AFTER INSERT
    ON DATA_REPOSITORY
    FOR EACH ROW
    WHEN(new.doc_status = 6 AND new.doc_class_name = 'BANK_STATEMENTS') DECLARE
    maxnum    INTEGER;
    maxnum2   INTEGER;
BEGIN
    SELECT MAX (num)
      INTO maxnum
      FROM w_campi_batch
     WHERE protocollo = :new.prot AND codice_campo BETWEEN 182 AND 187;

    INSERT_W_BODY_BATCH(2,187,maxnum,:new.prot);
    INSERT_W_BODY_BATCH(-1,183,maxnum,:new.prot);
    INSERT_W_BODY_BATCH(-3,182,maxnum,:new.prot);
    INSERT_W_BODY_BATCH(4,190,maxnum,:new.prot);
    INSERT_W_BODY_BATCH(5,67,maxnum,:new.prot);

	IF maxnum > 0 THEN
		FOR n IN 0 .. maxnum
		LOOP
			INSERT INTO w_body_batch (protocollo,
									  codiceutente,
									  codiceprog,
									  codicecolonna,
									  riga,
									  contenuto)
				 VALUES (:new.prot,
						 -4,
						 -1,
						 -2,
						 n,
						 ' ');
		END LOOP;
	END IF;
END;

create or replace TRIGGER "AI_W_BODY_BATCH_RM" AFTER INSERT
    ON DATA_REPOSITORY
    FOR EACH ROW
    WHEN(new.doc_status = 6 AND new.doc_class_name = 'REMITTANCE') DECLARE
    maxnum    INTEGER;
    maxnum2   INTEGER;
BEGIN
    SELECT MAX (num)
      INTO maxnum
      FROM w_campi_batch
     WHERE protocollo = :new.prot AND codice_campo IN (187,183,184,182,189);

    IF maxnum > 0 THEN
		INSERT_W_BODY_BATCH(2,187,maxnum,:new.prot);
		INSERT_W_BODY_BATCH(-1,183,maxnum,:new.prot);
		INSERT_W_BODY_BATCH(1,184,maxnum,:new.prot);
		INSERT_W_BODY_BATCH(-3,182,maxnum,:new.prot);
		INSERT_W_BODY_BATCH(3,189,maxnum,:new.prot);

		FOR n IN 0 .. maxnum
		  LOOP
			  INSERT INTO w_body_batch (protocollo,
										codiceutente,
										codiceprog,
										codicecolonna,
										riga,
										contenuto)
				   VALUES (:new.prot,
						   -4,
						   -1,
						   -2,
						   n,
						   ' ');
		END LOOP;
    END IF;
END;

Insert into O_BODY (CODICECOLONNA,LINK,TIPO,TITOLO) values ('2','H_CLIENTI','8','CUSTOMER');
Insert into O_BODY (CODICECOLONNA,LINK,TIPO,TITOLO) values ('-2',null,'0',' ');
Insert into O_BODY (CODICECOLONNA,LINK,TIPO,TITOLO) values ('-3','IMPORTO','1','AMOUNT');
Insert into O_BODY (CODICECOLONNA,LINK,TIPO,TITOLO) values ('3',null,'1','DISCOUNT');
Insert into O_BODY (CODICECOLONNA,LINK,TIPO,TITOLO) values ('-1',null,'0','REFERENCE');
Insert into O_BODY (CODICECOLONNA,LINK,TIPO,TITOLO) values ('1',null,'2','DATE');
Insert into O_BODY (CODICECOLONNA,LINK,TIPO,TITOLO) values ('4',null,'0','IBAN');
Insert into O_BODY (CODICECOLONNA,LINK,TIPO,TITOLO) values ('5',null,'0','DESCRIPTION');


Insert into O_BODYAZIENDE (CODICECOLONNA,CODICEDITTA,POSIZIONE,LARGHEZZA,PREFERENZA,OBBLIGATORIA) values ('2','0','0','200','0','0');
Insert into O_BODYAZIENDE (CODICECOLONNA,CODICEDITTA,POSIZIONE,LARGHEZZA,PREFERENZA,OBBLIGATORIA) values ('-2','0','7','0','0','0');
Insert into O_BODYAZIENDE (CODICECOLONNA,CODICEDITTA,POSIZIONE,LARGHEZZA,PREFERENZA,OBBLIGATORIA) values ('-3','0','3','100','0','0');
Insert into O_BODYAZIENDE (CODICECOLONNA,CODICEDITTA,POSIZIONE,LARGHEZZA,PREFERENZA,OBBLIGATORIA) values ('3','0','4','100','0','0');
Insert into O_BODYAZIENDE (CODICECOLONNA,CODICEDITTA,POSIZIONE,LARGHEZZA,PREFERENZA,OBBLIGATORIA) values ('1','0','1','80','0','0');
Insert into O_BODYAZIENDE (CODICECOLONNA,CODICEDITTA,POSIZIONE,LARGHEZZA,PREFERENZA,OBBLIGATORIA) values ('4','0','5','200','0','0');
Insert into O_BODYAZIENDE (CODICECOLONNA,CODICEDITTA,POSIZIONE,LARGHEZZA,PREFERENZA,OBBLIGATORIA) values ('5','0','6','200','0','0');
Insert into O_BODYAZIENDE (CODICECOLONNA,CODICEDITTA,POSIZIONE,LARGHEZZA,PREFERENZA,OBBLIGATORIA) values ('-1','0','2','200','0','0');


create or replace TRIGGER ai_B_SUBJECT_ALIAS
    AFTER INSERT
    ON DATA_REPOSITORY
    FOR EACH ROW
    WHEN (new.doc_status = 5 AND new.doc_class_name IN ('BANK_STATEMENTS', 'REMITTANCE'))
DECLARE
    validated    w_body_batch.contenuto%TYPE;
    recognized   S_BRAIN.VALORE%TYPE;
    n            NUMBER;
BEGIN
    SELECT contenuto
      INTO validated
      FROM w_body_batch
     WHERE PROTOCOLLO = :new.PROT AND CODICECOLONNA = -1;

    SELECT valore
      INTO recognized
      FROM s_brain
     WHERE PROTOCOLLO = :new.PROT AND nomecampo = 'NAME_REFERENCE';

    IF (validated IS NOT NULL AND recognized IS NOT NULL) THEN
        SELECT COUNT (*)
          INTO N
          FROM B_SUBJECT_ALIAS
         WHERE P_IVA = validated AND ALIAS = recognized;

        IF N = 0 THEN
            INSERT INTO B_SUBJECT_ALIAS (P_IVA,
                                         ALIAS,
                                         N,
                                         LAST_UPDATE)
                 VALUES (validated,
                         recognized,
                         1,
                         SYSDATE);
        ELSE
            UPDATE B_SUBJECT_ALIAS
               SET N = N + 1, LAST_UPDATE = SYSDATE
             WHERE P_IVA = validated AND ALIAS = recognized;
        END IF;
    END IF;
END;