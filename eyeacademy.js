///////////////////
// REGOLE CUSTOM //
///////////////////

// questo script PUO' contenere le seguenti funzioni
// invocabili dal codice Brain
// per riconoscimento:
// level1_custom(), level2_custom(), level3_custom(), level4_custom(), level5_custom()
// per validazione e training:
// validation_custom(), custom()

// tali funzioni devono assegnare un valore (intero) al risultato della chiamata
// CALL_STATUS
// assegnare il valore jcMgr.FATAL_CALL_STATUS in caso di errori non recuperabili che richiedono
// la propagazione fino a Brain

// dichiarazioni di classi Java
// NON MODIFICARE NON ELIMINARE
importPackage (Packages.it.idoq.lucystar.brain.rules.coded.model);
importPackage (Packages.it.idoq.lucystar.brain.rules.model);
importPackage (Packages.it.idoq.lucystar.brain.ejb.work);
importPackage (Packages.it.idoq.util);
//

// binding generali
// NON MODIFICARE NON ELIMINARE
//

// JC_MGR gestisce tutte le interazioni con la parte Java del motore
jcMgr = JC_MGR ;

// tipo di operazione che sta eseguendo lo script (1=recognize, 2=validate, 3=training)
var opType = OPTYPE ;
var patternMgr = PatternMgr.getInstance();

function level3() {
	logger.info("Level 3 OCR Remittances");
	
	logger.info("Finding Invoices Dates");
	searchAndFindInvoicesDates();
	
	logger.info("Finding Invoices Total");
	searchAndFindInvoicesTotal();
	
	logger.info("Finding Reference in Document");
	searchAndFindReferences();
	
	logger.info("Finding Currency for Document");
	searchAndFindCurrency();
	
	logger.info("Finding Total Amount Paid")
	searchAndFindTotalValue();
}

function searchAndFindInvoicesDates(){
	var elemKey = "INVOICE_DATE";
	var elemScore = 80;
	var ocrLabelElements = jcMgr.findOCRElems(elemKey);
	if (ocrLabelElements == null || ocrLabelElements.length == 0)
		return;
	
	var resultKey = "VALUE_DATE";
	var elemKeyRegEx = "DATE_FORMAT";
	var ocrElements = jcMgr.findOCRElems(elemKeyRegEx);
	if (ocrElements == null || ocrElements.length == 0)
		return;
	
	var i, y1 = ocrLabelElements[0].getTop() + 10, x1 = ocrLabelElements[0].getLeft() + 10;
	var x2 = x1 - 20;
	
	for (i=0; i<ocrElements.length; i++){
		var ocrElement = ocrElements[i];
		
		if(ocrElement.getTop() > y1/*  && ocrElement.getLeft() < x1 */ && ocrElement.getLeft() > x2){
			var realValue = ocrElement.getField().getText().substring(ocrElement.getOffset(), ocrElement.getOffset()+ocrElement.getLength() - 1);
			var ruleElement = new BaseRuleElement(resultKey, ocrElement, elemScore);
			ruleElement.setRoh(new BaseRuleObjHandle());
			ruleElement.setString(realValue);
				
			if(!jcMgr.hasResultWithLabelAndElement(resultKey,ruleElement))
				addResult(resultKey, ruleElement, elemScore);
		}
	}
}

function searchAndFindInvoicesTotal(){
	var elemKey = "INVOICE_TOTAL";
	var elemScore = 80;
	var ocrLabelElements = jcMgr.findOCRElems(elemKey);
	if (ocrLabelElements == null || ocrLabelElements.length == 0)
		return;
	
	var resultKey = "TRANSACTION_AMOUNT";
	var elemKeyRegEx = "AMOUNT";
	var ocrElements = jcMgr.findOCRElems(elemKeyRegEx);
	if (ocrElements == null || ocrElements.length == 0)
		return;
	
	var i, y1 = ocrLabelElements[0].getTop() + 10, x1 = ocrLabelElements[0].getRight() + 10;
	var x2 = x1 - 20;
	
	for (i=0; i<ocrElements.length; i++){
		var ocrElement = ocrElements[i];
		
		if(ocrElement.getTop() > y1  && ocrElement.getRight() < x1  && ocrElement.getRight() > x2){
			var realValue = ocrElement.getField().getText().substring(ocrElement.getOffset(), ocrElement.getOffset()+ocrElement.getLength() - 1);
			var ruleElement = new BaseRuleElement(resultKey, ocrElement, elemScore);
			ruleElement.setRoh(new BaseRuleObjHandle());
			addParam(elemKeyRegEx, realValue, ruleElement, resultKey);
		}
	}
}

function searchAndFindReferences(){
	var elemKey = "REFERENCE";
	var elemScore = 80;
	var ocrLabelElements = jcMgr.findOCRElems(elemKey);
	if (ocrLabelElements == null || ocrLabelElements.length == 0)
		return;
	
	var resultKey = "BANK_REFERENCE";
	var elemKeyRegEx = "BANK_REFERENCE";
	var ocrElements = jcMgr.findOCRElems(elemKeyRegEx);
	if (ocrElements == null || ocrElements.length == 0)
		return;
	
	var i, y1 = ocrLabelElements[0].getTop() + 10, x1 = ocrLabelElements[0].getLeft() + 10;
	var x2 = x1 - 20;
	
	for (i=0; i<ocrElements.length; i++){
		var ocrElement = ocrElements[i];
		
		if(ocrElement.getTop() > y1  && ocrElement.getLeft() < x1  && ocrElement.getLeft() > x2){
			var realValue = ocrElement.getField().getText().substring(ocrElement.getOffset(),ocrElement.getOffset()+ocrElement.getLength()-1);
			var ruleElement = new BaseRuleElement(resultKey, ocrElement, elemScore);
			ruleElement.setRoh(new BaseRuleObjHandle());
			ruleElement.setString(realValue);
			if(!jcMgr.hasResultWithLabelAndElement(resultKey, ruleElement))
				addResult(resultKey, ruleElement, elemScore);
		}
	}
}

function searchAndFindCurrency(){
	var elemKey = "VALUTA";
	var elemScore = 80;
	var ocrLabelElems = jcMgr.findOCRElems(elemKey);
	if (ocrLabelElems == null || ocrLabelElems.length == 0)
        return;
	
	var resultKey = "CURRENCY";
	var elemKeyRegEx = "VALUTA";
	var elemScore = 80;
	var ocrElems = jcMgr.findOCRElems(elemKeyRegEx);
	if (ocrElems == null || ocrElems.length == 0)
		return;
	var i;
	
	var y1 = (ocrElems[0].getPage().height)/3;
	
	for (i=0; i<ocrElems.length; i++) {
		var ocrElem = ocrElems[i];
		
		if(ocrElem.getTop() < y1) {
			var realValue = ocrElem.getField().getText().substring(ocrElem.getOffset(),ocrElem.getOffset()+ocrElem.getLength()-1);
			var ruleElement = new BaseRuleElement(resultKey, ocrElem, elemScore);
			ruleElement.setString(realValue);
			
			if(!jcMgr.hasResultWithLabelAndElement(resultKey, ruleElement))
				addResult(resultKey, ruleElement, elemScore);
		}
	}
}

function searchAndFindTotalValue(){
	var elemKey = "INVOICE_TOTAL";
	var elemScore = 80;
	var ocrLabelElems = jcMgr.findOCRElems(elemKey);
	if (ocrLabelElems == null || ocrLabelElems.length == 0)
        return;
	
	var resultKey = "IMPORTO";
	var elemKeyRegEx = "AMOUNT";
	var elemScore = 80;
	var ocrElems = jcMgr.findOCRElems(elemKeyRegEx);
	if (ocrElems == null || ocrElems.length == 0)
		return;
	var i;
	
	var y1 = (ocrElems[0].getPage().height)/3;
	var y2 = ocrLabelElems[0].getRight();
	
	for (i=0; i<ocrElems.length; i++) {
		var ocrElem = ocrElems[i];
		
		if(ocrElem.getTop() < y1 && ocrElem.getLeft() > y2) {
			var realValue = ocrElem.getField().getText().substring(ocrElem.getOffset(),ocrElem.getOffset()+ocrElem.getLength()-1);
			var ruleElement = new BaseRuleElement(resultKey, ocrElem, elemScore);
			ruleElement.setString(realValue);
			
			if(!jcMgr.hasResultWithLabelAndElement(resultKey, ruleElement))
				addResult(resultKey, ruleElement, elemScore);
		}
	}
}

function addParam(elemKey, realvalue, ruleElement, resultKey){
	var elemScore = 80;
	if(elemKey.equals("DATE")){
		var data = patternMgr.getData(realvalue, new Locale("en"), 1);
		if (data != null) {
			ruleElement.setDate(data);
			addResult(resultKey, ruleElement, elemScore);
		}
	}
	else if(elemKey.equals("AMOUNT")){
		var value = MathFunctions.value(realvalue);		
		if (value != null) {
			ruleElement.setValue(value);
			addResult(resultKey, ruleElement, elemScore);
		}
	}
}

function addResult(label, property, score) {
	var baseRulesResult = new BaseRulesResult(label,property,score);
    jcMgr.addResult(baseRulesResult);
}

function validation() {
	logger.info("Inside validation");
    var openingBalance = null ;
    try {
        openingBalance = jcMgr.getValidationElemsFor("OPENING_BALANCE") ;
    } catch(err) {
        throw err ;
    }
    if (openingBalance != null && openingBalance.length == 1 && openingBalance[0].getValue() != "" ) {		
	    openingBalance[0].setStatus(3)
	}	

    var closingBalance = null ;
    try {
        closingBalance = jcMgr.getValidationElemsFor("CLOSING_BALANCE") ;
    } catch(err) {
        throw err ;
    }
	
	if (closingBalance != null && closingBalance.length == 1 && closingBalance[0].getValue() != "") {		
	    closingBalance[0].setStatus(3)
	}

    if (openingBalance == null || closingBalance == null || openingBalance.length != 1 || closingBalance.length != 1 || openingBalance[0].getValue() == "" || closingBalance[0].getValue() == "") return ;

    var openingBalance = openingBalance[0].getAmount() ;
    var closingBalance = closingBalance[0].getAmount() ;
	
    var importo = null ;
    try {
        importo = jcMgr.getValidationElemsFor("IMPORTO") ;
    } catch(err) {
        throw err ;
    }

    if (importo.length != 1) return ;
	
	importo[0].setValue(closingBalance - openingBalance);
	
	importo[0].setStatus(3);

    CALL_STATUS = '1';
}

// trova una e una sola istanza di un item che rappresenta la quadratura (es. IMPONIBILE, IMPOSTA, TOTFAT...)
// con valore non nullo e importo diverso da zero
// se si trova piu' di una istanza si solleva un'eccezione
function findSingleValidationBalanceAmount(elemName, mustBeGreen) {
    var valElems = jcMgr.getValidationElemsFor(elemName) ;
    if (valElems == null || valElems.length == 0) {
        return null ;
    }
    
    var amountElems = new Array() ;
    var size = 0 ;
    var i ;
    for (i=0; i<valElems.length; i++) {
        var valElem = valElems[i] ;
        
        if (mustBeGreen) {
            // lo stato deve essere verde
            if (valElem.getStatus() != 3) continue ;
        } else {
            // flag false significa che lo stato deve essere minore di verde
            if (valElem.getStatus() >= 3) continue ;
        }
        if (valElem.getValue() == null || valElem.getAmount() == 0.0) continue ;
        
        amountElems[size] = valElem;
        size++ ;
    }
    if (amountElems.length > 1) throw jcMgr.FATAL_CALL_STATUS ;
    if (amountElems.length == 0) {
        return null ;
    }
    
    return amountElems[0] ;
}