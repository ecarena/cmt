function validation() {
	logger.info("Inside Bank Statements validation");
   
    var currency = null, importo = null, dataDoc = null, accID = null;
    try{
        importo = jcMgr.getValidationElemsFor("IMPORTO");
        currency = jcMgr.getValidationElemsFor("VALUTA");
        dataDoc = jcMgr.getValidationElemsFor("DATADOC");
        accID = jcMgr.getValidationElemsFor("ACCOUNT_ID");
    } catch(err){
        throw err;
    }
    if(!currency[0].getValue().equals("")) {
        currency[0].setStatus(3);
    }
    
    if(!importo[0].getValue().equals("")) {
        importo[0].setStatus(3);
    }
    
    if(dataDoc[0].getDate() != null){
        dataDoc[0].setStatus(3);
    }
    
	var re = new RegExp("[0-9]+");
	
    if(re.test(accID[0].getValue())){
        accID[0].setStatus(3);
    }

    if(currency[0].getStatus() == 3 && importo[0].getStatus() == 3 && dataDoc[0].getStatus() == 3 && accID[0].getStatus() == 3){
        jcMgr.setDocumentConfidence(3);
    }

    CALL_STATUS = '1';
}

