///////////////////
// REGOLE CUSTOM //
///////////////////

// questo script PUO' contenere le seguenti funzioni
// invocabili dal codice Brain
// per riconoscimento:
// level1_custom(), level2_custom(), level3_custom(), level4_custom(), level5_custom()
// per validazione e training:
// validation_custom(), custom()

// tali funzioni devono assegnare un valore (intero) al risultato della chiamata
// CALL_STATUS
// assegnare il valore jcMgr.FATAL_CALL_STATUS in caso di errori non recuperabili che richiedono
// la propagazione fino a Brain

// dichiarazioni di classi Java
// NON MODIFICARE NON ELIMINARE
importPackage (Packages.it.idoq.lucystar.brain.rules.coded.model);
importPackage (Packages.it.idoq.lucystar.brain.rules.model);
importPackage (Packages.it.idoq.lucystar.brain.ejb.work);
importPackage (Packages.it.idoq.util);
import "recognition.js";

//

// binding generali
// NON MODIFICARE NON ELIMINARE
//

// JC_MGR gestisce tutte le interazioni con la parte Java del motore
jcMgr = JC_MGR ;

// tipo di operazione che sta eseguendo lo script (1=recognize, 2=validate, 3=training)
var opType = OPTYPE ;
var patternMgr = PatternMgr.getInstance();

function level3() {
	logger.info("Level 3 OCR");
	
	searchAndFindDocumentDate();   //Cerca la data del documento
	searchAndFindTransactionsDescriptions();
	searchAndFindOpeningBalance(); //Cerca il saldo iniziale
	searchAndFindClosingBalance(); //Cerca il saldo finale
	searchAndFindTransactions();   //Cerca le transazioni
	searchAndFindDateEntries();    //Cerca le date delle transazioni
	searchAndFindDateEntriesValue(); //Cerca le date da quando sono valide le transazioni
	searchAndFindCurrency();	   //Cerca la valuta delle transazioni (unica per documento)
	searchAndFindBankReference();  //Cerca il riferimento alla banca
	searchAndFindCustomerRef();	   //Cerca il nome del cliente	
	computeTotalAmount();   // Calcola l'importo totale come differenza tra saldo chiusura e saldo apertura
}

function computeTotalAmount() {
	var ob = jcMgr.getResultsFor("OPENING_BALANCE") ;
	var cb = jcMgr.getResultsFor("CLOSING_BALANCE") ;
	
	if (ob == null || ob.length == 0 || cb == null || cb.length == 0)
        return;
	
	var ruleElement = new BaseRuleElement("IMPORTO", null, 80);
	ruleElement.setValue(cb[0].getProperty().getValue() - ob[0].getProperty().getValue());
	addResult("IMPORTO", ruleElement, 80);	
}

function searchAndFindOpeningBalance(){
	var label = "OPENING_LABEL";
	var ocrLabelElements = findOCR(label);
	
	if (ocrLabelElements == null || ocrLabelElements.length == 0){
		logger.info("Non ci sono LABEL");
		return;
	}

	var resultKey = "OPENING_BALANCE", elemKey = "AMOUNT";
	var ocrElements = findOCR(elemKey);
	var i, j, y1 = ocrLabelElements[0].getTop() - 10, y2 = ocrLabelElements[0].getTop() + 10;
	
	for (i=0; i<ocrElements.length; i++) {
		var ocrElement = ocrElements[i];
		if(ocrElement.getTop() < y2 && ocrElement.getTop() > y1) {
			insertParameters(resultKey, elemKey, ocrElement);
		}
	}
}

function searchAndFindClosingBalance(){
	var elemKey = "CLOSING_LABEL";
	var elemScore = 80;
	var ocrLabelElements = jcMgr.findOCRElems(elemKey);
	if (ocrLabelElements == null || ocrLabelElements.length == 0)
        return;

	var resultKey = "CLOSING_BALANCE";
	var elemKeyRegEx = "AMOUNT";
	var ocrElements = jcMgr.findOCRElems(elemKeyRegEx);
	var i, j, y1 = ocrLabelElements[0].getTop() - 10, y2 = ocrLabelElements[0].getTop() + 10;
	
	for (i=0; i<ocrElements.length; i++) {
		var ocrElement = ocrElements[i];
		
		if(ocrElement.getTop() < y2 && ocrElement.getTop() > y1) {
			var realValue = ocrElement.getField().getText().substring(ocrElement.getOffset(), ocrElement.getOffset()+ocrElement.getLength() - 1).replace(/\s+/g,"");
			var ruleElement = new BaseRuleElement(resultKey, ocrElement, elemScore);
			ruleElement.setString(realValue);
			
			if(!jcMgr.hasResultWithLabelAndElement(resultKey,ruleElement))
				addParam(elemKeyRegEx, realValue, ruleElement, resultKey)
		}
	}
}

function searchAndFindTransactions(){
	var elemKey = "TRANSACTION_LABEL";
	var elemScore = 80;

	var ocrLabelElements = jcMgr.findOCRElems(elemKey);
	
	var resultKey = "TRANSACTION_AMOUNT";
	var elemKeyRegEx = "AMOUNT";
	
	var ocrElements = jcMgr.findOCRElems(elemKeyRegEx);
	if (ocrElements == null || ocrElements.length == 0)
        return;
	var i, j;
	
	for (j=0; j<ocrLabelElements.length; j++){
		
		var y1 = ocrLabelElements[j].getTop() - 10;
		var y2 = ocrLabelElements[j].getTop() + 10;
		var np = ocrLabelElements[j].getField().pageNumber();
		
		for (i=0; i<ocrElements.length; i++) {
			var ocrElement = ocrElements[i];
		
			if(ocrElement.getTop() < y2 && ocrElement.getTop() > y1 && ocrElement.getField().pageNumber() == np) {
				var realValue = ocrElement.getField().getText().substring(ocrElement.getOffset(),ocrElement.getOffset()+ocrElement.getLength()-1).replace(/\s+/g,"");
				var ruleElement = new BaseRuleElement(resultKey, ocrElement, elemScore);
				ruleElement.setRoh(new BaseRuleObjHandle());
				addParam(elemKeyRegEx, realValue, ruleElement, resultKey)
			}
		}
	}
}

function searchAndFindDateEntries(){
	var elemKey = "ENTRY_DATE_LABEL";
	var elemScore = 80;

	var ocrLabelElems = jcMgr.findOCRElems(elemKey);
	if (ocrLabelElems == null || ocrLabelElems.length == 0)
        return;
	
	var resultKey = "ENTRY_DATE";
	var elemKeyRegEx = "DATE";
	
	var ocrElems = jcMgr.findOCRElems(elemKeyRegEx);
	if (ocrElems == null || ocrElems.length == 0)
        return;
	var i, j;
	
	for (j=0; j<ocrLabelElems.length; j++){
		
		var y1 = ocrLabelElems[j].getTop() - 10;
		var y2 = ocrLabelElems[j].getTop() + 10;
		var np = ocrLabelElems[j].getField().pageNumber();
		
		for (i=0; i<ocrElems.length; i++) {
			var ocrElem = ocrElems[i];
		
			if(ocrElem.getTop() < y2 && ocrElem.getTop() > y1 && ocrElem.getField().pageNumber() == np) {
				var realValue = ocrElem.getField().getText().substring(ocrElem.getOffset(),ocrElem.getOffset()+ocrElem.getLength()-1).replace(/\s+/g,"");
				var ruleElement = new BaseRuleElement(resultKey, ocrElem, elemScore);
				ruleElement.setRoh(new BaseRuleObjHandle());
				addParam(elemKeyRegEx, realValue, ruleElement, resultKey)
			}
		}
	}
}

function searchAndFindDateEntriesValue(){
	var elemKey = "VALUE_DATE_LABEL";
	var elemScore = 80;

	var ocrLabelElems = jcMgr.findOCRElems(elemKey);
	if (ocrLabelElems == null || ocrLabelElems.length == 0)
        return;
	
	var resultKey = "VALUE_DATE";
	var elemKeyRegEx = "DATE";
	
	var ocrElems = jcMgr.findOCRElems(elemKeyRegEx);
	if (ocrElems == null || ocrElems.length == 0)
        return;
	var i, j;
	
	for (j=0; j<ocrLabelElems.length; j++){
		
		var y1 = ocrLabelElems[j].getTop() - 10;
		var y2 = ocrLabelElems[j].getTop() + 10;
		var np = ocrLabelElems[j].getField().pageNumber();
		
		for (i=0; i<ocrElems.length; i++) {
			var ocrElem = ocrElems[i];
		
			if(ocrElem.getTop() < y2 && ocrElem.getTop() > y1 && ocrElem.getField().pageNumber() == np) {
				var realValue = ocrElem.getField().getText().substring(ocrElem.getOffset(),ocrElem.getOffset()+ocrElem.getLength()-1).replace(/\s+/g,"");
				var ruleElement = new BaseRuleElement(resultKey, ocrElem, elemScore);
				ruleElement.setRoh(new BaseRuleObjHandle());
				addParam(elemKeyRegEx, realValue, ruleElement, resultKey)
			}
		}
	}
}

function searchAndFindCurrency(){
	var elemKey = "VALUTA";
	var elemScore = 80;
	var ocrLabelElems = jcMgr.findOCRElems(elemKey);
	if (ocrLabelElems == null || ocrLabelElems.length == 0)
        return;
	
	var resultKey = "CURRENCY";
	var elemKeyRegEx = "VALUTA";
	var elemScore = 80;
	
	var ocrElems = jcMgr.findOCRElems(elemKeyRegEx);
	if (ocrElems == null || ocrElems.length == 0)
        return;
	var i;
	
	var y1 = ocrLabelElems[0].getTop() - 10;
	var y2 = ocrLabelElems[0].getTop() + 10;
	
	for (i=0; i<ocrElems.length; i++) {
		var ocrElem = ocrElems[i];
		
		if(ocrElem.getTop() < y2 && ocrElem.getTop() > y1) {
			var realValue = ocrElem.getField().getText().substring(ocrElem.getOffset(),ocrElem.getOffset()+ocrElem.getLength()-1).replace(/\s+/g,"");
			var ruleElement = new BaseRuleElement(resultKey, ocrElem, elemScore);
			ruleElement.setString(realValue);
			
			if(!jcMgr.hasResultWithLabelAndElement(resultKey, ruleElement))
				addResult(resultKey, ruleElement, elemScore);
		}
	}
}

function searchAndFindBankReference(){
	var elemKey = "BANK_LABEL";
	var elemScore = 80;

	var ocrLabelElems = jcMgr.findOCRElems(elemKey);
	if (ocrLabelElems == null || ocrLabelElems.length == 0)
        return;
	
	var resultKey = "BANK_REFERENCE";
	var elemKeyRegEx = "BANK_REFERENCE";
	
	var ocrElems = jcMgr.findOCRElems(elemKeyRegEx);
	if (ocrElems == null || ocrElems.length == 0)
        return;
	var i, j;
	
	for (j=0; j<ocrLabelElems.length; j++){
		
		var y1 = ocrLabelElems[j].getTop() - 10;
		var y2 = ocrLabelElems[j].getTop() + 10;
		var np = ocrLabelElems[j].getField().pageNumber();
		
		for (i=0; i<ocrElems.length; i++) {
			var ocrElem = ocrElems[i];
		
			if(ocrElem.getTop() < y2 && ocrElem.getTop() > y1 && ocrElem.getField().pageNumber() == np) {
				var realValue = ocrElem.getField().getText().substring(ocrElem.getOffset(),ocrElem.getOffset()+ocrElem.getLength()-1).replace(/\s+/g,"");
				var ruleElement = new BaseRuleElement(resultKey, ocrElem, elemScore);
				ruleElement.setRoh(new BaseRuleObjHandle());
				ruleElement.setString(realValue);
			
				if(!jcMgr.hasResultWithLabelAndElement(resultKey,ruleElement))
					addResult(resultKey, ruleElement, elemScore);
			}
		}
	}
}

function searchAndFindCustomerRef(){
	var elemKey = "NAME_LABEL";
	var elemScore = 80;

	var ocrLabelElems = jcMgr.findOCRElems(elemKey);
	if (ocrLabelElems == null || ocrLabelElems.length == 0)
        return;
	
	var resultKey = "NAME_REFERENCE";
	var resultKey2 = "TAX_NUMBER";
	var elemKeyRegEx = "NAME_REFERENCE";
	
	var ocrElems = jcMgr.findOCRElems(elemKeyRegEx);
	if (ocrElems == null || ocrElems.length == 0)
        return;
	var i, j;
	
	for (j=0; j<ocrLabelElems.length; j++){
		
		var ocrLabelElem = ocrLabelElems[j];
		
		var y1 = ocrLabelElem.getTop() + 10;
		var y2 = ocrLabelElem.getTop() + 200;
		var np = ocrLabelElem.getField().pageNumber();
		
		var found = false;
		
		for (i=0; i<ocrElems.length; i++) {
			var ocrElem = ocrElems[i];
		
			if(ocrElem.getTop() < y2 && ocrElem.getTop() > y1 && ocrElem.getField().pageNumber() == np) {
				var realValue = ocrElem.getField().getText().substring(ocrElem.getOffset(),ocrElem.getOffset()+ocrElem.getLength()-1);

				var query = "select P_IVA from B_SUBJECT_ALIAS where ALIAS = '"+realValue+"';";
				logger.info("Query Executed: " + query);
				var queryResults = null ;
				try {
					queryResults = coreDb.select(query);
				} catch(err) {
					logger.error("Error executing query: "+err);
				throw err;
				}
				if (queryResults == null || queryResults.length == 0) {
					logger.info("Query results empty");
					continue;
				} 
				if (queryResults.length > 1) {
					logger.info("Multiple query results");
					continue;
				} 
			
				var ruleElement = new BaseRuleElement(resultKey, ocrElem, elemScore);
				ruleElement.setRoh(new BaseRuleObjHandle());
				ruleElement.setString(realValue);
				
//				if(!jcMgr.hasResultWithLabelAndElement(resultKey,ruleElement))
					addResult(resultKey, ruleElement, elemScore);
			
			    logger.info("Aggiunge piva =" + queryResults[0]["P_IVA"]);
				
				var ruleElement = new BaseRuleElement(resultKey2, ocrElem, elemScore);
				ruleElement.setRoh(new BaseRuleObjHandle());
				ruleElement.setString(queryResults[0]["P_IVA"]);

//				if(!jcMgr.hasResultWithLabelAndElement(resultKey2,ruleElement)) {
					addResult("TAX_NUMBER", ruleElement, elemScore);
					found = true;
//				}
								
				break;
			}
		}
		
		if (!found) {
			logger.info("Aggiungo placeholder");
			
			var ruleElement = new BaseRuleElement(resultKey, ocrLabelElem, elemScore);
			ruleElement.setRoh(new BaseRuleObjHandle());
			ruleElement.setString("-");
//			if(!jcMgr.hasResultWithLabelAndElement(resultKey2,ruleElement))
				addResult(resultKey, ruleElement, elemScore);

			var ruleElement = new BaseRuleElement(resultKey2, ocrLabelElem, elemScore);
			ruleElement.setRoh(new BaseRuleObjHandle());
			ruleElement.setString("-");
//			if(!jcMgr.hasResultWithLabelAndElement(resultKey2,ruleElement))
				addResult(resultKey2, ruleElement, elemScore);
		}
	}
}

function searchAndFindDocumentDate(){
	var elemKey = "DATE_LABEL";
	var elemScore = 80;

	var ocrLabelElems = jcMgr.findOCRElems(elemKey);
	if (ocrLabelElems == null || ocrLabelElems.length == 0)
        return;
	
	var resultKey = "DATA_DOC";
	var elemKeyRegEx = "DATE";
	var ocrElems = jcMgr.findOCRElems(elemKeyRegEx);
	if (ocrElems == null || ocrElems.length == 0)
        return;
	var y1 = (ocrElems[0].getPage().height)/2;
	var y2 = ocrLabelElems[0].getRight();
	var i;
	for (i=0; i<ocrElems.length; i++) {
		var ocrElem = ocrElems[i];
		
		if( ocrElem.getTop() < y1 && ocrElem.getLeft() > y2) {
			var realValue = ocrElem.getField().getText().substring(ocrElem.getOffset(),ocrElem.getOffset()+ocrElem.getLength()-1).replace(/\s+/g,"");
			var ruleElement = new BaseRuleElement(resultKey, ocrElem, elemScore);
			ruleElement.setRoh(new BaseRuleObjHandle());
			addParam(elemKeyRegEx, realValue, ruleElement, resultKey)
		}
	}
}

function searchAndFindTransactionsDescriptions(){
	
	var elemKey = "DESCRIPTION_LABEL";
	var elemScore = 80;

	var ocrLabelElements = jcMgr.findOCRElems(elemKey);
	
	logger.info("abc" + ocrLabelElements.length);

	var resultKey = "DESCRIPTION";
	var elemKeyRegEx = "DESCRIPTION";
	
	var ocrElements = jcMgr.findOCRElems(elemKeyRegEx);
	if (ocrElements == null || ocrElements.length == 0)
        return;
	var i, j;
	
	for (j=0; j<ocrLabelElements.length; j++){
		
		var y1 = ocrLabelElements[j].getTop() - 20;
		var y2 = ocrLabelElements[j].getTop() + 20;
		var np = ocrLabelElements[j].getField().pageNumber();
		
		for (i=0; i<ocrElements.length; i++) {
			var ocrElement = ocrElements[i];
		
			if(ocrElement.getTop() < y2 && ocrElement.getTop() > y1 && ocrElement.getField().pageNumber() == np) {
				var realValue = ocrElement.getField().getText().substring(ocrElement.getOffset(),ocrElement.getOffset()+ocrElement.getLength()-1).replace(/\s+/g,"");
				var ruleElement = new BaseRuleElement(resultKey, ocrElement, elemScore);
				ruleElement.setRoh(new BaseRuleObjHandle());
				addParam(elemKeyRegEx, realValue, ruleElement, resultKey)
			}
		}
	}
}

function addParam(elemKey, realvalue, ruleElement, resultKey){
	var elemScore = 80;
	if(elemKey.equals("DATE")){
		var data = patternMgr.getData(realvalue, new Locale("en"), 1);
		if (data != null) {
			ruleElement.setDate(data);
//			if(!jcMgr.hasResultWithLabelAndElement(resultKey, ruleElement))
				addResult(resultKey, ruleElement, elemScore);
		}
	}
	else if(elemKey.equals("AMOUNT")){
		var value = MathFunctions.value(realvalue);		
		if (value != null) {
			ruleElement.setValue(value);
//			if(!jcMgr.hasResultWithLabelAndElement(resultKey, ruleElement))
				addResult(resultKey, ruleElement, elemScore);
		}
	}
}

function addResult(label, property, score) {
	var baseRulesResult = new BaseRulesResult(label,property,score);
    jcMgr.addResult(baseRulesResult);
}

function validation() {
	logger.info("Inside validation");
    var openingBalance = null ;
    try {
        openingBalance = jcMgr.getValidationElemsFor("OPENING_BALANCE") ;
    } catch(err) {
        throw err ;
    }
    if (openingBalance != null && openingBalance.length == 1 && openingBalance[0].getValue() != "" ) {		
	    openingBalance[0].setStatus(3)
	}	

    var closingBalance = null ;
    try {
        closingBalance = jcMgr.getValidationElemsFor("CLOSING_BALANCE") ;
    } catch(err) {
        throw err ;
    }
	
	if (closingBalance != null && closingBalance.length == 1 && closingBalance[0].getValue() != "") {		
	    closingBalance[0].setStatus(3)
	}

    if (openingBalance == null || closingBalance == null || openingBalance.length != 1 || closingBalance.length != 1 || openingBalance[0].getValue() == "" || closingBalance[0].getValue() == "") return ;

    var openingBalance = openingBalance[0].getAmount() ;
    var closingBalance = closingBalance[0].getAmount() ;
	
    var importo = null ;
    try {
        importo = jcMgr.getValidationElemsFor("IMPORTO") ;
    } catch(err) {
        throw err ;
    }

    if (importo.length != 1) return ;
	
	if (importo != null && importo[0].getValue() != "" ) {		
	    importo[0].setStatus(3)
	}
	else {
		return null;
	}
	
	importo[0].setValue(closingBalance - openingBalance);
	
	importo[0].setStatus(3);
	
	try {
        valuta = jcMgr.getValidationElemsFor("VALUTA") ;
    } catch(err) {
        throw err ;
    }

	if (valuta != null && valuta[0].getValue() != "" ) {		
	    valuta[0].setStatus(3)
	}
	else {
		return null;
	}
	
	jcMgr.setDocumentConfidence(3);

    CALL_STATUS = '1';
}

// trova una e una sola istanza di un item che rappresenta la quadratura (es. IMPONIBILE, IMPOSTA, TOTFAT...)
// con valore non nullo e importo diverso da zero
// se si trova piu' di una istanza si solleva un'eccezione
function findSingleValidationBalanceAmount(elemName, mustBeGreen) {
    var valElems = jcMgr.getValidationElemsFor(elemName) ;
    if (valElems == null || valElems.length == 0) {
        return null ;
    }
    
    var amountElems = new Array() ;
    var size = 0 ;
    var i ;
    for (i=0; i<valElems.length; i++) {
        var valElem = valElems[i] ;
        
        if (mustBeGreen) {
            // lo stato deve essere verde
            if (valElem.getStatus() != 3) continue ;
        } else {
            // flag false significa che lo stato deve essere minore di verde
            if (valElem.getStatus() >= 3) continue ;
        }
        if (valElem.getValue() == null || valElem.getAmount() == 0.0) continue ;
        
        amountElems[size] = valElem;
        size++ ;
    }
    if (amountElems.length > 1) throw jcMgr.FATAL_CALL_STATUS ;
    if (amountElems.length == 0) {
        return null ;
    }
    
    return amountElems[0] ;
}