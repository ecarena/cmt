///////////////////
// REGOLE CUSTOM //
///////////////////

// questo script PUO' contenere le seguenti funzioni
// invocabili dal codice Brain
// per riconoscimento:
// level1_custom(), level2_custom(), level3_custom(), level4_custom(), level5_custom()
// per validazione e training:
// validation_custom(), custom()

// tali funzioni devono assegnare un valore (intero) al risultato della chiamata
// CALL_STATUS
// assegnare il valore jcMgr.FATAL_CALL_STATUS in caso di errori non recuperabili che richiedono
// la propagazione fino a Brain

// dichiarazioni di classi Java
// NON MODIFICARE NON ELIMINARE
importPackage (Packages.it.idoq.lucystar.brain.rules.coded.model);
importPackage (Packages.it.idoq.lucystar.brain.rules.model);
importPackage (Packages.it.idoq.lucystar.brain.ejb.work);
importPackage (Packages.it.idoq.util);
//

// binding generali
// NON MODIFICARE NON ELIMINARE
//

// JC_MGR gestisce tutte le interazioni con la parte Java del motore
jcMgr = JC_MGR ;

// tipo di operazione che sta eseguendo lo script (1=recognize, 2=validate, 3=training)
var opType = OPTYPE ;
var patternMgr = PatternMgr.getInstance();

function level1_custom() {
	logger.info("Level 1 OCR");
	var ocrLabelElements = jcMgr.findOCRElems("CITIBANK");
	if (ocrLabelElements != null && ocrLabelElements.length > 0) {
		jcMgr.addSubjectWithVAT("1234567890");
	}
}
