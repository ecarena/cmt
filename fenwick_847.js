///////////////////
// REGOLE CUSTOM //
///////////////////

// questo script PUO' contenere le seguenti funzioni
// invocabili dal codice Brain
// per riconoscimento:
// level1_custom(), level2_custom(), level3_custom(), level4_custom(), level5_custom()
// per validazione e training:
// validation_custom(), custom()

// tali funzioni devono assegnare un valore (intero) al risultato della chiamata
// CALL_STATUS
// assegnare il valore jcMgr.FATAL_CALL_STATUS in caso di errori non recuperabili che richiedono
// la propagazione fino a Brain

// dichiarazioni di classi Java
// NON MODIFICARE NON ELIMINARE
importPackage (Packages.it.idoq.lucystar.brain.rules.coded.model);
importPackage (Packages.it.idoq.lucystar.brain.rules.model);
importPackage (Packages.it.idoq.lucystar.brain.ejb.work);
importPackage (Packages.it.idoq.util);
//

// binding generali
// NON MODIFICARE NON ELIMINARE
//

// JC_MGR gestisce tutte le interazioni con la parte Java del motore
jcMgr = JC_MGR ;

// tipo di operazione che sta eseguendo lo script (1=recognize, 2=validate, 3=training)
var opType = OPTYPE ;
var patternMgr = PatternMgr.getInstance();

function level3() {
	logger.info("Level 3 OCR Fenwick Remittances");
	
	logger.info("Finding Internal Reference in Document");
	searchAndFindReference();
	
	logger.info("Finding Invoices Dates");
	searchAndFindInvoicesDates();
	
	logger.info("Finding Total Amount Paid");
	searchAndFindTotalValue();
	
	logger.info("Finding Suppliers References");
	searchAndFindSupplierReferences();
	
	logger.info("Finding Invoices Total");
	searchAndFindInvoicesTotal();
	
	logger.info("Finding Discount");
	searchAndFindDiscount();
	
	logger.info("Finding Remittance Date");
	searchAndFindDocumentDate();
}

function searchAndFindReference(){
	evaluateSingleLabelSingleLineElement("FENWICK_REF", "BANK_REFERENCE", "NUMDOC");
}

function searchAndFindInvoicesDates(){
	evaluateSingleLabelMultipleElements("DATE_LABEL", "DATE", "DATE");
}

function searchAndFindTotalValue(){
	evaluateSingleLabelSingleElement("TOTAL", "AMOUNT", "IMPORTO");
}

function searchAndFindSupplierReferences(){
	evaluateSingleLabelMultipleElementsWithParameters("REFERENCE_LABEL", "TEN_NUMBERS", "REFERENCE",-10,100);
	//evaluateSingleLabelMultipleElementsAlignedToLeftParameters("REFERENCE_LABEL", "TEN_NUMBERS", "REFERENCE",-10,100);
}

function searchAndFindInvoicesTotal(){
	evaluateSingleLabelMultipleElementsAlignedToRightParameters("INVOICE_TOTAL", "AMOUNT", "TRANSACTION_AMOUNT",10,100);
}

function searchAndFindDiscount(){
	evaluateSingleLabelMultipleElementsAlignedToRightParameters("DISCOUNT", "AMOUNT", "DISCOUNT",-100,-10);
}

function searchAndFindDocumentDate(){
	evaluateSingleLabelSingleElement("DATE_SENTENCE_LABEL", "DATE", "DATADOC");
}

function validation() {
	logger.info("Inside validation");

    var fenwickRef = null ;
    try {
        fenwickRef = jcMgr.getValidationElemsFor("NUMDOC") ;
    } catch(err) {
		logger.info("An Error has occured: "+ err.message);
        throw err;
    }
    if (fenwickRef != null && fenwickRef.length == 1 && fenwickRef[0].getValue() != "" )
	    fenwickRef[0].setStatus(3)

    var dataDoc = null ;
    try {
        dataDoc = jcMgr.getValidationElemsFor("DATADOC") ;
    } catch(err) {
		logger.info("An Error has occured: "+ err.message);
        throw err ;
    }
	
	if (dataDoc != null && dataDoc.length == 1 && dataDoc[0].getValue() != "")
	    dataDoc[0].setStatus(3)

    if (fenwickRef == null || dataDoc == null || fenwickRef.length != 1 || dataDoc.length != 1 || fenwickRef[0].getValue() == "" || dataDoc[0].getValue() == "") return ;
	
	var importo = null
    try {
        importo = jcMgr.getValidationElemsFor("IMPORTO") ;
    } catch(err) {
		logger.info("An Error has occured: "+ err.message);
        throw err ;
    }

    if (importo.length != 1) return ;
	
	if (importo != null && importo[0].getValue() != "" )		
	    importo[0].setStatus(3)
	
	var fenwickCurrency = null ;
    try {
        fenwickCurrency = jcMgr.getValidationElemsFor("VALUTA") ;
    } catch(err) {
		logger.info("An Error has occured: "+ err.message);
        throw err;
    }
    if (fenwickCurrency != null && fenwickCurrency.length == 1 && fenwickCurrency[0].getValue() != "" )
	    fenwickCurrency[0].setStatus(3)
	
	jcMgr.setDocumentConfidence(3);

    CALL_STATUS = '1';
}


// trova una e una sola istanza di un item che rappresenta la quadratura (es. IMPONIBILE, IMPOSTA, TOTFAT...)
// con valore non nullo e importo diverso da zero
// se si trova piu' di una istanza si solleva un'eccezione
function findSingleValidationBalanceAmount(elemName, mustBeGreen) {
    var valElems = jcMgr.getValidationElemsFor(elemName) ;
    if (valElems == null || valElems.length == 0) {
        return null ;
    }
    
    var amountElems = new Array() ;
    var size = 0 ;
    var i ;
    for (i=0; i<valElems.length; i++) {
        var valElem = valElems[i] ;
        
        if (mustBeGreen) {
            // lo stato deve essere verde
            if (valElem.getStatus() != 3) continue ;
        } else {
            // flag false significa che lo stato deve essere minore di verde
            if (valElem.getStatus() >= 3) continue ;
        }
        if (valElem.getValue() == null || valElem.getAmount() == 0.0) continue ;
        
        amountElems[size] = valElem;
        size++ ;
    }
    if (amountElems.length > 1) throw jcMgr.FATAL_CALL_STATUS ;
    if (amountElems.length == 0) {
        return null ;
    }
    
    return amountElems[0] ;
}