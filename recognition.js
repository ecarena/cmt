///////////////////
// REGOLE CUSTOM //
///////////////////

// dichiarazioni di classi Java
// NON MODIFICARE NON ELIMINARE
importPackage (Packages.it.idoq.lucystar.brain.rules.coded.model);
importPackage (Packages.it.idoq.lucystar.brain.rules.model);
importPackage (Packages.it.idoq.lucystar.brain.ejb.work);
importPackage (Packages.it.idoq.util);

//Questo file contiene i metodi usate dagli script di riconoscimento per i vari documenti

var patternMgr = PatternMgr.getInstance();

function findOCR(key){
	return jcMgr.findOCRElems(key);
}

function isNullOrEmpty(elementsList){
	if (elementsList == null) { 
		logger.info("Lista nulla!");
		return true;
	}
	else if (elementsList.length == 0) {
		logger.info("Lista vuota!");
		return true;
	}
	else return false;
}

/* function checkParameters(ocrElement){
	var realValue = ocrElement.getField().getText().substring(ocrElement.getOffset(), ocrElement.getOffset()+ocrElement.getLength() - 1);
	if(realValue.charAt(ocrElement.getOffset)=="\("){
		if(realValue.charAt(ocrElement.getLength()-1) tion== "\)")
	}
} */

function insertParameters(resultKey, elemKeyRegEx, ocrElement) {
	var score = 80;
	var realValue = ocrElement.getField().getText().substring(ocrElement.getOffset(), ocrElement.getOffset()+ocrElement.getLength() - 1);
	var ruleElement = new BaseRuleElement(resultKey, ocrElement, score);
	ruleElement.setString(realValue);
	ruleElement.setRoh(new BaseRuleObjHandle());
	
	if(elemKeyRegEx.equals("DATE") || elemKeyRegEx.equals("AMOUNT")){
		addParam(elemKeyRegEx, realValue, ruleElement, resultKey)
	}
	else {
		addResult(resultKey, ruleElement, score);
	}
}

function insertParametersSpace(resultKey, elemKeyRegEx, ocrElement){
	var score = 80;
	var realValue = ocrElement.getField().getText().substring(ocrElement.getOffset(), ocrElement.getOffset()+ocrElement.getLength()-1).replace(/\s+/g,"");
	var ruleElement = new BaseRuleElement(resultKey, ocrElement, score);
	ruleElement.setString(realValue);
	
	if(elemKeyRegEx.equals("DATE") || elemKeyRegEx.equals("AMOUNT")){
		addParam(elemKeyRegEx, realValue, ruleElement, resultKey)
	}
	else {
		addResult(resultKey, ruleElement, score);
	}
}

function evaluateSingleLabelSingleElement(label, elemKeyRegEx, resultKey){	
	evaluateSingleLabelSingleElementWithParameters(label, elemKeyRegEx, resultKey, -10, 10, 0);
}

function evaluateMultipleLabelMultipleElement(label, elemKeyRegEx, resultKey){
	evaluateMultipleLabelMultipleElementWithParameters(label, elemKeyRegEx, resultKey, -10, 10, 0);
}

function evaluateSingleLabelMultipleElements(label, elemKeyRegEx, resultKey){
	evaluateSingleLabelMultipleElementsWithParameters(label, elemKeyRegEx, resultKey, -10);
}

function evaluateSingleLabelMultipleElementsAndCheck(label, elemKeyRegEx, checkRegEx, resultKey){
	evaluateSingleLabelMultipleElementsWithParametersAndCheck(label, elemKeyRegEx, checkRegEx, resultKey, -10);
}

function evaluateSingleLabelMultipleElementsAlignedToRightAndCheck(label, elemKeyRegEx, checkRegEx, resultKey){
	evaluateSingleLabelMultipleElementsAlignedToRightWithParametersAndCheck(label, elemKeyRegEx, checkRegEx, resultKey, -10);
}

function evaluateSingleLabelMultipleElementsAlignedToRightAndCheckIfNot(label, elemKeyRegEx, checkRegEx, resultKey, range){
	evaluateSingleLabelMultipleElementsAlignedToRightWithParametersAndCheckIfNot(label, elemKeyRegEx, checkRegEx, resultKey, -10);
}

function evaluateSingleLabelSingleLineElement(label, elemKeyRegEx, resultKey){
	evaluateSingleLabelSingleLineElementWithParameters(label, elemKeyRegEx, resultKey, -10, 10);
}

function evaluateSingleLabelMultipleElementsNotAligned(label, elemKeyRegEx, resultKey){
	evaluateSingleLabelMultipleElementsNotAlignedParameters(label, elemKeyRegEx, resultKey, -10, 10);
}

function evaluateSingleLabelMultipleElementsNotAlignedToLeft(label, elemKeyRegEx, resultKey){
	evaluateSingleLabelMultipleElementsNotAlignedToLeftParameters(label, elemKeyRegEx, resultKey, -10, 10);
}

function evaluateSingleLabelMultipleElementsAlignedToRight(label, elemKeyRegEx, resultKey){
	evaluateSingleLabelMultipleElementsAlignedToRightParameters(label, elemKeyRegEx, resultKey, -20, 20);
}

function evaluateSingleLabelMultipleElementsAlignedToLeft(label, elemKeyRegEx, resultKey){
	evaluateSingleLabelMultipleElementsAlignedToLeftParameters(label, elemKeyRegEx, resultKey, -10, 10);
}

function evaluateSingleLabelSingleLineElementOnBottom(label, elemKeyRegEx, resultKey){
	evaluateSingleLabelSingleLineElementOnBottomWithParameters(label, elemKeyRegEx, resultKey, -10, 10);
}

function searchAndFindCrossedElements(firstLabel, secondLabel, elemKeyRegEx, resultKey){
	searchAndFindCrossedElementsParameters(firstLabel, secondLabel, elemKeyRegEx, resultKey, -30);
}

function evaluateSingleLabelMultipleElementsAlignedToRightNotFirstPage(label, elemKeyRegEx, resultKey){
	evaluateSingleLabelMultipleElementsAlignedToRightNotFirstPageParameters(label, elemKeyRegEx, resultKey, -10, 10);
}

function evaluateSingleLabelSingleBottomRightElement(label, elemKeyRegEx, resultKey){
	evaluateSingleLabelSingleBottomRightElementWithParameters(label, elemKeyRegEx, resultKey, -10)
}

function evaluateLastValue(label, elemKeyRegEx, resultKey){
	evaluateLastValueWithParameters(label, elemKeyRegEx, resultKey, -30, 30);
}

function evaluateSingleLabelMultipleElementsAlignedToRightNotFirstPageAndCheck(label, elemKeyRegEx, checkRegEx, resultKey){
	evaluateSingleLabelMultipleElementsAlignedToRightNotFirstPageAndCheckParameters(label, elemKeyRegEx, checkRegEx, resultKey, -10)
}

function evaluateSingleLabelMultipleElementsAlignedToLeftNotFirstPage(label, elemKeyRegEx, resultKey){
	evaluateSingleLabelMultipleElementsAlignedToLeftNotFirstPageParameters(label, elemKeyRegEx, resultKey, -10, 10);
}

function evaluateSingleLabelSingleElementWithParameters(label, elemKeyRegEx, resultKey, mindy, maxdy, mindx){	
	var ocrLabelElements = jcMgr.findOCRElems(label);
	var ocrElements = jcMgr.findOCRElems(elemKeyRegEx);

	if (!isNullOrEmpty(ocrLabelElements) && !isNullOrEmpty(ocrElements)){
		var i, j, y1 = ocrLabelElements[0].getTop() + mindy, y2 = ocrLabelElements[0].getTop() + maxdy, x1 = ocrLabelElements[0].getRight() + mindx;
				
		for (var i=0;i<ocrElements.length;i++){
			var ocrElement = ocrElements[i];
			if(elemKeyRegEx != "VALUTA"){
				if(ocrElement.getTop() < y2 && ocrElement.getTop() > y1 && ocrElement.getLeft() > x1){
					insertParameters(resultKey, elemKeyRegEx, ocrElement);
					break;
				}
			}
			else if(ocrElement.getTop() < y2 && ocrElement.getTop() > y1){
				insertParameters(resultKey, elemKeyRegEx, ocrElement);
				break;
			}
		}
	}
}

function evaluateMultipleLabelMultipleElementWithParameters(label, elemKeyRegEx, resultKey, mindy, maxdy, mindx){
	var ocrLabelElements = jcMgr.findOCRElems(label);
	var ocrElements = jcMgr.findOCRElems(elemKeyRegEx);
	var i, j;
	
	if (!isNullOrEmpty(ocrLabelElements) && !isNullOrEmpty(ocrElements)){
		for (j=0; j<ocrLabelElements.length; j++){
			var y1 = ocrLabelElements[j].getTop() + mindy;
			var y2 = ocrLabelElements[j].getTop() + maxdy;
			var np = ocrLabelElements[j].getField().pageNumber();
			
			for (i=0; i<ocrElements.length; i++) {
				var ocrElement = ocrElements[i];
				
				if(ocrElement.getTop() < y2 && ocrElement.getTop() > y1 && ocrElement.getField().pageNumber() == np) 
					insertParameters(resultKey, elemKeyRegEx, ocrElement);
			}
		}
	}
}

function evaluateSingleLabelMultipleElementsWithParameters(label, elemKeyRegEx, resultKey, range){
	var ocrLabelElements = jcMgr.findOCRElems(label);
	if (isNullOrEmpty(ocrLabelElements))
		logger.info("No single label");
	
	var ocrElements = jcMgr.findOCRElems(elemKeyRegEx);
	var i, y1 = ocrLabelElements[0].getTop() + range, x1 = ocrLabelElements[0].getLeft(), x2 = ocrLabelElements[0].getRight();
	
	if (ocrElements != null) {
		for (i=0; i<ocrElements.length; i++){
			var ocrElement = ocrElements[i];
			if(ocrElement.getTop() > y1 && ocrElement.getLeft() > x1 && ocrElement.getLeft() < x2){
				insertParameters(resultKey, elemKeyRegEx, ocrElement); 
			}
		}
	}
}

function evaluateSingleLabelMultipleElementsWithParametersAndCheck(label, elemKeyRegEx, checkRegEx, resultKey, range){
	var ocrLabelElements = jcMgr.findOCRElems(label);
	if (isNullOrEmpty(ocrLabelElements))
		logger.info("No single label");
	
	var ocrElements = jcMgr.findOCRElems(elemKeyRegEx);
	var checkElements = jcMgr.findOCRElems(checkRegEx);
	var i, y1 = ocrLabelElements[0].getTop() + range, x1 = ocrLabelElements[0].getLeft() + range, x2 = ocrLabelElements[0].getRight() - range;
	
	if (ocrElements != null) {
		for (i=0; i<ocrElements.length; i++){
			var ocrElement = ocrElements[i];
			if(ocrElement.getTop() > y1 && ocrElement.getLeft() > x1 && ocrElement.getLeft() < x2){
				var k, cy1 = ocrElement.getTop() + range, cy2 = ocrElement.getTop() - range;
				for (k=0; k<checkElements.length; k++){
					var checkElement = checkElements[k];
					if(checkElement.getTop() > cy1 && checkElement.getTop() < cy2 && (!checkRegEx.equals("DATE") || parseDate(checkElement) != null)){
						insertParameters(resultKey, elemKeyRegEx, ocrElement); 
						break;
					}
				}
			}
		}
	}
}

function evaluateSingleLabelMultipleElementsAlignedToRightWithParametersAndCheck(label, elemKeyRegEx, checkRegEx, resultKey, range){
	var ocrLabelElements = jcMgr.findOCRElems(label);
	if (isNullOrEmpty(ocrLabelElements))
		logger.info("No single label");
	
	var ocrElements = jcMgr.findOCRElems(elemKeyRegEx);
	var checkElements = jcMgr.findOCRElems(checkRegEx);
	var i, y1 = ocrLabelElements[0].getTop() + range, x1 = ocrLabelElements[0].getRight() + range, x2 = ocrLabelElements[0].getRight() - range;
	
	if (ocrElements != null) {
		for (i=0; i<ocrElements.length; i++){
			var ocrElement = ocrElements[i];
			if(ocrElement.getTop() > y1 && ocrElement.getRight() > x1 && ocrElement.getRight() < x2){
				var k, cy1 = ocrElement.getTop() + range, cy2 = ocrElement.getTop() - range;
				for (k=0; k<checkElements.length; k++){
					var checkElement = checkElements[k];
					if(checkElement.getTop() > cy1 && checkElement.getTop() < cy2 && parseDate(checkElement) != null){
						insertParameters(resultKey, elemKeyRegEx, ocrElement); 
						break;
					}
				}
			}
		}
	}
}

function evaluateSingleLabelMultipleElementsAlignedToRightWithParametersAndCheckIfNot(label, elemKeyRegEx, checkRegEx, resultKey, range){
	var ocrLabelElements = jcMgr.findOCRElems(label);
	if (isNullOrEmpty(ocrLabelElements))
		logger.info("No single label");
	
	var ocrElements = jcMgr.findOCRElems(elemKeyRegEx);
	var checkElements = jcMgr.findOCRElems(checkRegEx);
	var i, y1 = ocrLabelElements[0].getTop() + range, x1 = ocrLabelElements[0].getRight() - range, x2 = ocrLabelElements[0].getRight() + range;
	
	if (ocrElements != null) {
		for (i=0; i<ocrElements.length; i++){
			var ocrElement = ocrElements[i];
			if(ocrElement.getTop() > y1 && ocrElement.getRight() > x1 && ocrElement.getRight() < x2){
				var k, cy1 = ocrElement.getTop() + range, cy2 = ocrElement.getTop() - range;
				for (k=0; k<checkElements.length; k++){
					var checkElement = checkElements[k];
					if(checkElement.getTop() > cy1 && checkElement.getTop() < cy2 && parseDate(checkElement) == null){
						insertParameters(resultKey, elemKeyRegEx, ocrElement); 
						break;
					}
				}
			}
		}
	}
}

function evaluateSingleLabelSingleLineElementWithParameters(label, elemKeyRegEx, resultKey, mindy, maxdy){
	var ocrLabelElements = jcMgr.findOCRElems(label);
	if (isNullOrEmpty(ocrLabelElements))
		logger.info("No single label");
	
	var ocrElements = jcMgr.findOCRElems(elemKeyRegEx);
	var i, y1 = ocrLabelElements[0].getTop() + mindy, y2 = ocrLabelElements[0].getTop() + maxdy, x = ocrLabelElements[0].getRight();
	
	if (ocrElements != null) {
		for (var i=0;i<ocrElements.length;i++){
			var ocrElement = ocrElements[i];
			if(ocrElement.getTop() < y2 && ocrElement.getTop() > y1 && ocrElement.getLeft() > x) {
				insertParameters(resultKey, elemKeyRegEx, ocrElement);
				break;
			}
		}
	}
}

function evaluateSingleLabelMultipleElementsNotAlignedParameters(label, elemKeyRegEx, resultKey, range){
	var ocrLabelElements = jcMgr.findOCRElems(label);
	if (isNullOrEmpty(ocrLabelElements))
		logger.info("No single label");
	
	var ocrElements = jcMgr.findOCRElems(elemKeyRegEx);
	var i, y1 = ocrLabelElements[0].getTop() + range, x1 = ocrLabelElements[0].getLeft(), x2 = ocrLabelElements[0].getRight();
	
	if (ocrElements != null) {
		for (i=0; i<ocrElements.length; i++){
			var ocrElement = ocrElements[i];
			if(ocrElement.getTop() > y1 && ocrElement.getLeft() < x1 && ocrElement.getLeft() < x2){
				insertParameters(resultKey, elemKeyRegEx, ocrElement); 
			}
		}
	}
}

function evaluateSingleLabelMultipleElementsNotAlignedToLeftParameters(label, elemKeyRegEx, resultKey, mindy, maxdy){
	var ocrLabelElements = jcMgr.findOCRElems(label);
	if (isNullOrEmpty(ocrLabelElements))
		logger.info("No single label");
	
	var ocrElements = jcMgr.findOCRElems(elemKeyRegEx);
	var y1 = ocrLabelElements[0].getTop() + mindy, x1 = ocrLabelElements[0].getLeft()-40, x2 = ocrLabelElements[0].getRight()+30;
	
	if (ocrElements != null) {
		for (var i=0; i<ocrElements.length; i++){
			var ocrElement = ocrElements[i];
			if(ocrElement.getTop() > y1 && ocrElement.getRight() < x2 && ocrElement.getLeft() > x1 ){
				insertParameters(resultKey, elemKeyRegEx, ocrElement); 
			}
		}
	}
}

function evaluateSingleLabelMultipleElementsAlignedToRightParameters(label, elemKeyRegEx, resultKey, mindx, maxdx){
	var ocrLabelElements = jcMgr.findOCRElems(label);
	if (isNullOrEmpty(ocrLabelElements))
		logger.info("No single label");
	
	var ocrElements = jcMgr.findOCRElems(elemKeyRegEx);
	var y1 = ocrLabelElements[0].getBottom(), x1 = ocrLabelElements[0].getRight()+mindx, x2 = ocrLabelElements[0].getRight()+maxdx;
	
	if (ocrElements != null) {
		for (var i=0; i<ocrElements.length; i++){
			var ocrElement = ocrElements[i];
			if(ocrElement.getTop() > y1 && ocrElement.getRight() < x2 && ocrElement.getRight() > x1 ){
				insertParameters(resultKey, elemKeyRegEx, ocrElement); 
			}
		}
	}
}

function evaluateLastValueWithParameters(label, elemKeyRegEx, resultKey, mindx, maxdx){
	var ocrLabelElements = jcMgr.findOCRElems(label);
	if (isNullOrEmpty(ocrLabelElements))
		logger.info("No single label");
	
	var ocrElements = jcMgr.findOCRElems(elemKeyRegEx);
	var y1 = ocrLabelElements[0].getBottom(), x1 = ocrLabelElements[0].getRight()+mindx, x2 = ocrLabelElements[0].getRight()+maxdx;
	
	if (ocrElements != null) {
		for (var i=ocrElements.length-1; i>0; i--){
			var ocrElement = ocrElements[i];
			if(ocrElement.getRight() < x2 && ocrElement.getRight() > x1 ){
				insertParameters(resultKey, elemKeyRegEx, ocrElement); 
				break;
			}
		}
	}
}

function evaluateSingleLabelMultipleElementsAlignedToLeftParameters(label, elemKeyRegEx, resultKey, mindx, maxdx){
	var ocrLabelElements = jcMgr.findOCRElems(label);
	if (isNullOrEmpty(ocrLabelElements))
		logger.info("No single label");
	
	var ocrElements = jcMgr.findOCRElems(elemKeyRegEx);
	var y1 = ocrLabelElements[0].getBottom(), x1 = ocrLabelElements[0].getLeft()+mindx, x2 = ocrLabelElements[0].getLeft()+maxdx;
	
	if (ocrElements != null) {
		for (var i=0; i<ocrElements.length; i++){
			var ocrElement = ocrElements[i];
			if(ocrElement.getTop() > y1 && ocrElement.getLeft() < x2 && ocrElement.getLeft() > x1 ){
				insertParameters(resultKey, elemKeyRegEx, ocrElement); 
			}
		}
	}
}

function evaluateSingleLabelSingleLineElementOnBottomWithParameters(label, elemKeyRegEx, resultKey, mindy, maxdy){
	var ocrLabelElements = jcMgr.findOCRElems(label);
	if (isNullOrEmpty(ocrLabelElements))
		logger.info("No single label");
	
	var ocrElements = jcMgr.findOCRElems(elemKeyRegEx);
	var i, y1 = ocrLabelElements[0].getTop() + maxdy, x = ocrLabelElements[0].getRight();
	
	if (ocrElements != null) {
		for (var i=0;i<ocrElements.length;i++){
			var ocrElement = ocrElements[i];
			if(ocrElement.getTop() > y1 && ocrElement.getRight() < x) {
				insertParameters(resultKey, elemKeyRegEx, ocrElement);
				break;
			}
		}
	}
}

function searchAndFindCrossedElementsParameters(columnLabel, rowLabel, elemKeyRegEx, resultKey, range){
	var firstOCRLabels = jcMgr.findOCRElems(columnLabel);
	var secondOCRLabels = jcMgr.findOCRElems(rowLabel);
	
	if(isNullOrEmpty(firstOCRLabels)){
		logger.info("No Column label");
	}
	else if(isNullOrEmpty(secondOCRLabels)){
		logger.info("No Row Label");
	} else logger.info("Found All Labels!");
	
	var columnY1 = firstOCRLabels[0].getRight() + range;
	var columnY2 = firstOCRLabels[0].getRight() - range;
	var rowY1 = secondOCRLabels[0].getTop() + range;
	var rowY2 = secondOCRLabels[0].getTop() - range;
	
	var ocrElements = findOCR(elemKeyRegEx);
	if (ocrElements != null) {
		for(var i=0;i<ocrElements.length;i++){
			var ocrElement = ocrElements[i];
			if(ocrElement.getRight() > columnY1 && ocrElement.getRight() < columnY2 && ocrElement.getTop() > rowY1 && ocrElement.getTop() < rowY2){
				insertParameters(resultKey, elemKeyRegEx, ocrElement);
			}
		}
	}
}

function evaluateSingleLabelMultipleElementsAlignedToRightNotFirstPageParameters(label, elemKeyRegEx, resultKey, mindx, maxdx){
	var ocrLabelElements = jcMgr.findOCRElems(label);
	if (isNullOrEmpty(ocrLabelElements))
		logger.info("No single label");
	
	var ocrElements = jcMgr.findOCRElems(elemKeyRegEx);
	var y1 = ocrLabelElements[1].getBottom(), x1 = ocrLabelElements[1].getRight()+mindx, x2 = ocrLabelElements[1].getRight()+maxdx;
	
	if (ocrElements != null) {
		for (var i=0; i<ocrElements.length; i++){
			var ocrElement = ocrElements[i];
			if(ocrElement.getTop() > y1 && ocrElement.getRight() < x2 && ocrElement.getRight() > x1 && ocrElement.getField().pageNumber() != 0 && i != ocrElements.length-1){
				insertParameters(resultKey, elemKeyRegEx, ocrElement); 
			}
		}
	}
}

function evaluateSingleLabelSingleBottomRightElementWithParameters(label, elemKeyRegEx, resultKey, range){
	var ocrLabelElements = jcMgr.findOCRElems(label);
	if (isNullOrEmpty(ocrLabelElements))
		logger.info("No single label");
	
	var ocrElements = jcMgr.findOCRElems(elemKeyRegEx);
	var i, y1 = ocrLabelElements[0].getTop() + range, x = ocrLabelElements[0].getRight();
	
	if (ocrElements != null) {
		for (var i=0;i<ocrElements.length;i++){
			var ocrElement = ocrElements[i];
			if(ocrElement.getTop() > y1 && ocrElement.getRight() > x) {
				insertParameters(resultKey, elemKeyRegEx, ocrElement);
				break;
			}
		}
	}
}

function evaluateSingleLabelMultipleElementsAlignedToRightNotFirstPageAndCheckParameters(label, elemKeyRegEx, checkRegEx, resultKey, range){
	var ocrLabelElements = jcMgr.findOCRElems(label);
	if (isNullOrEmpty(ocrLabelElements))
		logger.info("No single label");
	
	var ocrElements = jcMgr.findOCRElems(elemKeyRegEx);
	var checkElements = jcMgr.findOCRElems(checkRegEx);
	
	if(isNullOrEmpty(ocrElements)){
		logger.info("First Label NULL");
	} else if(isNullOrEmpty(checkElements)){
		logger.info("Second Label NULL");
	}

	var i, y1, x1, x2;

	for (i=0; i<ocrLabelElements.length; i++){
		if (ocrLabelElements[i].getField().pageNumber() != 0) {
			y1 = ocrLabelElements[i].getTop() + range, x1 = ocrLabelElements[i].getRight() + range, x2 = ocrLabelElements[i].getRight() - range;
		}
	}
	
	if (ocrElements != null) {
		for (i=0; i<ocrElements.length; i++){
			var ocrElement = ocrElements[i];
			if(ocrElement.getTop() > y1 && ocrElement.getRight() > x1 && ocrElement.getRight() < x2){
				var k, cy1 = ocrElement.getTop() + range, cy2 = ocrElement.getTop() - range;
				for (k=0; k<checkElements.length; k++){
					var checkElement = checkElements[k];
					if(checkElement.getTop() > cy1 && checkElement.getTop() < cy2 && ocrElement.getField().pageNumber() == checkElement.getField().pageNumber() && ocrElement.getField().pageNumber() != 0 && /*(!checkRegEx.equals("DATE") || parseDate(checkElement) != null)*/ i != ocrElements.length-1){
						insertParameters(resultKey, elemKeyRegEx, ocrElement); 
						break;
					}
				}
			}
		}
	}
}

function evaluateSingleLabelMultipleElementsAlignedToLeftNotFirstPageParameters(label, elemKeyRegEx, resultKey, mindx, maxdx){
	var ocrLabelElements = jcMgr.findOCRElems(label);
	if (isNullOrEmpty(ocrLabelElements))
		logger.info("No single label");
	
	var ocrElements = jcMgr.findOCRElems(elemKeyRegEx);
	var y1 = ocrLabelElements[1].getBottom(), x1 = ocrLabelElements[1].getLeft()+mindx, x2 = ocrLabelElements[1].getLeft()+maxdx;
	
	if (ocrElements != null) {
		for (var i=0; i<ocrElements.length; i++){
			var ocrElement = ocrElements[i];
			if(ocrElement.getTop() > y1 && ocrElement.getLeft() < x2 && ocrElement.getLeft() > x1 && ocrElement.getField().pageNumber() != 0){
				insertParameters(resultKey, elemKeyRegEx, ocrElement); 
			}
		}
	}
}

function addParam(elemKey, realvalue, ruleElement, resultKey){
	var elemScore = 80;
	if(elemKey.equals("DATE")){
		var dataStr = jcMgr.getPropertyForSubject("tipodata");
		var data = 0;
		if (dataStr != null) {
			data = parseInt(dataStr);
		}
		var data = patternMgr.getData(realvalue, new Locale("en"), data);
		if (data != null) {
			ruleElement.setDate(data);
			addResult(resultKey, ruleElement, elemScore);
			return data;
		}
	}
	else if(elemKey.equals("AMOUNT")){
		var value = 0;
		if (!realvalue.equals(".00")) {
			if (realvalue.length > 2 && realvalue.substring( 0, 1 ).equals("(") && realvalue.substring(realvalue.length - 1, realvalue.length ).equals(")")) {
				value = -MathFunctions.value(realvalue.substring(1,realvalue.length - 1));
			} else {
				value = MathFunctions.value(realvalue);
			}
		}
			
		if (value != null) {
			ruleElement.setValue(value);
			addResult(resultKey, ruleElement, elemScore);
			return value;
		}
	}
}

function addResult(label, property, score) {
	var baseRulesResult = new BaseRulesResult(label,property,score);
    jcMgr.addResult(baseRulesResult);
}

/* function validation() {
	logger.info("Inside validation");
	
    var importo = null ;
    try {
        importo = jcMgr.getValidationElemsFor("IMPORTO") ;
    } catch(err) {
        throw err ;
    }
	
	importo[0].setStatus(3);
	 var documentDate = null;
	try {
        documentDate = jcMgr.getValidationElemsFor("DATADOC") ;
    } catch(err) {
        throw err ;
    } 
	
	jcMgr.setDocumentConfidence(3);

    CALL_STATUS = '1';
}  */

function parseDate(ocrElement) {
	var realValue = ocrElement.getField().getText().substring(ocrElement.getOffset(), ocrElement.getOffset()+ocrElement.getLength() - 1);
	var dataStr = jcMgr.getPropertyForSubject("tipodata");
	var data = 0;
	if (dataStr != null) {
		data = parseInt(dataStr);
	}
	return patternMgr.getData(realValue, new Locale("en"), data);
}

