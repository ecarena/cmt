///////////////////
// REGOLE CUSTOM //
///////////////////

// questo script PUO' contenere le seguenti funzioni
// invocabili dal codice Brain
// per riconoscimento:
// level1_custom(), level2_custom(), level3_custom(), level4_custom(), level5_custom()
// per validazione e training:
// validation_custom(), custom()

// tali funzioni devono assegnare un valore (intero) al risultato della chiamata
// CALL_STATUS
// assegnare il valore jcMgr.FATAL_CALL_STATUS in caso di errori non recuperabili che richiedono
// la propagazione fino a Brain

// dichiarazioni di classi Java
// NON MODIFICARE NON ELIMINARE
importPackage (Packages.it.idoq.lucystar.brain.rules.coded.model);
importPackage (Packages.it.idoq.lucystar.brain.rules.model);
importPackage (Packages.it.idoq.lucystar.brain.ejb.work);
importPackage (Packages.it.idoq.util);
//

// binding generali
// NON MODIFICARE NON ELIMINARE
//

// JC_MGR gestisce tutte le interazioni con la parte Java del motore
jcMgr = JC_MGR ;

// tipo di operazione che sta eseguendo lo script (1=recognize, 2=validate, 3=training)
var opType = OPTYPE ;
var patternMgr = PatternMgr.getInstance();

function level3() {
	logger.info("Level 3 OCR Boots Remittances");
	
	logger.info("Finding Invoices Dates");
	searchAndFindInvoicesDates();
	
	logger.info("Finding Invoices Total");
	searchAndFindInvoicesTotal();
	
	logger.info("Finding Internal Reference in Document");
	searchAndFindReference();
	
	logger.info("Finding Currency for Document");
	searchAndFindCurrency();
	
	logger.info("Finding Total Amount Paid");
	searchAndFindTotalValue();
	
	logger.info("Finding Remittance Date");
	searchAndFindDocumentDate();
	
	logger.info("Finding Suppliers References");
	searchAndFindSupplierReferences();
}

function searchAndFindInvoicesDates(){
	evaluateSingleLabelMultipleElements("INVOICE_DATE", "DATE", "DATE");
}

function searchAndFindInvoicesTotal(){
	evaluateSingleLabelMultipleElementsAndCheck("AMOUNT_PAID", "AMOUNT", "DATE", "TRANSACTION_AMOUNT");
}

function searchAndFindCurrency(){
	evaluateSingleLabelSingleElement("VALUTA", "VALUTA", "VALUTA");
}

function searchAndFindTotalValue(){
	evaluateSingleLabelSingleElement("TOTAL", "AMOUNT", "IMPORTO");
}

function searchAndFindDocumentDate(){
	evaluateSingleLabelSingleLineElement("DATE_LABEL", "DATE", "DATADOC");
}

function searchAndFindReference(){
	evaluateSingleLabelSingleLineElement("SUPPLIER", "BANK_REFERENCE", "NUMDOC");
}

function searchAndFindSupplierReferences(){
	evaluateSingleLabelMultipleElementsNotAligned("INTERNAL_REFERENCE_LABEL", "BANK_REFERENCE", "REFERENCE");
}

function addResult(label, property, score) {
	var baseRulesResult = new BaseRulesResult(label,property,score);
    jcMgr.addResult(baseRulesResult);
}

function validation() {
	logger.info("Inside validation");
    var openingBalance = null ;
    try {
        openingBalance = jcMgr.getValidationElemsFor("OPENING_BALANCE") ;
    } catch(err) {
        throw err ;
    }
    if (openingBalance != null && openingBalance.length == 1 && openingBalance[0].getValue() != "" ) {		
	    openingBalance[0].setStatus(3)
	}	

    var closingBalance = null ;
    try {
        closingBalance = jcMgr.getValidationElemsFor("CLOSING_BALANCE") ;
    } catch(err) {
        throw err ;
    }
	
	if (closingBalance != null && closingBalance.length == 1 && closingBalance[0].getValue() != "") {		
	    closingBalance[0].setStatus(3)
	}

    if (openingBalance == null || closingBalance == null || openingBalance.length != 1 || closingBalance.length != 1 || openingBalance[0].getValue() == "" || closingBalance[0].getValue() == "") return ;

    var openingBalance = openingBalance[0].getAmount() ;
    var closingBalance = closingBalance[0].getAmount() ;
	
    var importo = null ;
    try {
        importo = jcMgr.getValidationElemsFor("IMPORTO") ;
    } catch(err) {
        throw err ;
    }

    if (importo.length != 1) return ;
	
	importo[0].setValue(closingBalance - openingBalance);
	
	importo[0].setStatus(3);

    CALL_STATUS = '1';
}

// trova una e una sola istanza di un item che rappresenta la quadratura (es. IMPONIBILE, IMPOSTA, TOTFAT...)
// con valore non nullo e importo diverso da zero
// se si trova piu' di una istanza si solleva un'eccezione
function findSingleValidationBalanceAmount(elemName, mustBeGreen) {
    var valElems = jcMgr.getValidationElemsFor(elemName) ;
    if (valElems == null || valElems.length == 0) {
        return null ;
    }
    
    var amountElems = new Array() ;
    var size = 0 ;
    var i ;
    for (i=0; i<valElems.length; i++) {
        var valElem = valElems[i] ;
        
        if (mustBeGreen) {
            // lo stato deve essere verde
            if (valElem.getStatus() != 3) continue ;
        } else {
            // flag false significa che lo stato deve essere minore di verde
            if (valElem.getStatus() >= 3) continue ;
        }
        if (valElem.getValue() == null || valElem.getAmount() == 0.0) continue ;
        
        amountElems[size] = valElem;
        size++ ;
    }
    if (amountElems.length > 1) throw jcMgr.FATAL_CALL_STATUS ;
    if (amountElems.length == 0) {
        return null ;
    }
    
    return amountElems[0] ;
}